class RemoveImageUrlFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :image_url, :string
    remove_column :users, :access_secret, :string
  end
end
