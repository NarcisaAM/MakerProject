class AddUserToVenture < ActiveRecord::Migration[5.0]
  def change
    add_reference :ventures, :user, foreign_key: true
  end
end
