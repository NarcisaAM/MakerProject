class AddVentureToUserStory < ActiveRecord::Migration[5.0]
  def change
    add_reference :user_stories, :venture, foreign_key: true
  end
end
