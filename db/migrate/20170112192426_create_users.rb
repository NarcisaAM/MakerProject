# frozen_string_literal: true
class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :image_url
      t.string :access_token
      t.string :access_secret
      t.string :uid

      t.timestamps
    end
  end
end
