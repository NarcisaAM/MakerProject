# frozen_string_literal: true
class CreateVentures < ActiveRecord::Migration[5.0]
  def change
    create_table :ventures do |t|
      t.string :name
      t.text :description
    end
  end
end
