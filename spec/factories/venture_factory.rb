FactoryGirl.define do
  factory :venture do
    user
    name "A new name"
    description "A new description"
  end
end
