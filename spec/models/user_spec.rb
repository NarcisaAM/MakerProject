require "rails_helper"

RSpec.describe User, type: :model do

  describe "Web authentication" do
    describe ".from_oauth" do
      subject do
        User.create(name: "Gigi",
                  access_token: "asqweasf34234",
                  access_secret: "asraweqasd")
      end
      context "Given this user" do
        it "DB will return this user" do
          expect(User.from_oauth(subject.name, subject.access_token, subject.access_secret)).to eql(subject)
        end
      end
    end
  end

end
