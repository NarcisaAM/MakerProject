require "rails_helper"

RSpec.describe UserStory, type: :model do

  describe "UserStory attributes" do
    subject do
      UserStory.new(title: "Add some model tests",
                    status: 1
                    )
    end

  end

  it { is_expected.to respond_to(:title) }
  it { is_expected.to respond_to(:status) }

end
