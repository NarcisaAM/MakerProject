require "rails_helper"

RSpec.describe Api::V1::VenturesController, type: :controller do

  describe "GET 'show'" do
    let(:user) { create(:user) }
    let(:auth_headers) { request.headers["Authorization"] = "Token token=#{user.auth_token}" }
    let(:venture) { create(:venture) }

    before :each do
      get :show, params: { id: venture.id }, headers: auth_headers, format: :json
    end

    it "Should be successful" do
      expect(response).to be_success
    end

    it "Should return the correct venture when the correct id is given" do
      body = JSON.parse(response.body)
      expect(body["id"]).to eq(venture.id)
    end

  end

end
