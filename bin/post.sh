#!/bin/bash

if [[ -z "$2" ]]; then
	echo "$0 <post file> <route>" 
	exit 1
fi 

curl -LvH "Content-Type: application/json" --data @$1 http://localhost:3000/$2
