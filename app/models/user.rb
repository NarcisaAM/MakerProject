# frozen_string_literal: true
class User < ApplicationRecord
  has_many :ventures

  def self.from_oauth(name, access_token, access_token_secret)
    user = find_or_create_by_token(access_token, access_token_secret, name)
    user.update_attribute(:auth_token, SecureRandom.urlsafe_base64)
    user
  end

  # mobile
  def self.from_mobile_oauth(access_token, access_token_secret)
    client = twitter_client(access_token, access_token_secret)
    user = find_or_create_by_token(access_token, access_token_secret, client.user.name)

    user.update_attribute(:auth_token, SecureRandom.urlsafe_base64)
    user
  end

  private

    def self.find_or_create_by_token(access_token, access_token_secret, name)
      find_or_create_by(access_token: access_token) do |new_user|
        new_user.name = name
        new_user.access_token = access_token
        new_user.access_secret = access_token_secret
      end
    end

    def twitter_client(access_token, access_token_secret)
      secrets = Rails.application.secrets
      client = Twitter::REST::Client.new do |config|
        config.consumer_key = secrets.fetch(:twitter_consumer_key)
        config.consumer_secret     = secrets.fetch(:twitter_consumer_secret)
        config.access_token        = access_token
        config.access_token_secret = access_token_secret
      end
    end
end
