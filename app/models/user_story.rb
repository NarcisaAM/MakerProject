class UserStory < ApplicationRecord
  belongs_to :venture
  has_one :user, through: :venture
  validates :title, presence: true
end
