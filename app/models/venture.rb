# frozen_string_literal: true
class Venture < ApplicationRecord
  belongs_to :user
  has_many :user_stories, dependent: :destroy
  validates :name, presence: true
end
