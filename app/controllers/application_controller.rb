# class ApplicationController < ActionController::Base
class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  # respond_to :json

  protected

    def render_errors(message: {}, status: 400)
      render json: { message: message }, status: status
    end

    def require_authorization
      #binding.pry
      current_user || (head 401)
    end

    def current_user
      current_user ||= authenticate_or_request_with_http_token do |token|
        User.find_by(auth_token: token)
      end
    end
end
