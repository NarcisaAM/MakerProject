class Api::V1::OmniauthCallbacksController < ApplicationController
  def twitter
    secrets = Rails.application.secrets
    user = User.from_oauth(auth_hash.info.name, auth_hash.credentials.token, auth_hash.credentials.secret)
    cookies[:auth_token] = user.auth_token
    redirect_to secrets.fetch(:web_redirect_url)
  end

  private
    def auth_hash
      request.env["omniauth.auth"]
    end
end
