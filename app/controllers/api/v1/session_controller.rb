class Api::V1::SessionController < ApplicationController
  def create
    access_token = params[:access_token]
    access_secret = params[:access_secret]

    user = User.from_mobile_oauth(access_token, access_secret)
    render json: user.to_json(only: [:name, :auth_token])

    rescue Twitter::Error::Unauthorized
      render_errors(message: "Something went wrong - Unauthorized access.", status: 401)
    rescue ActionController::ParameterMissing
      render_errors(message: "Something went wrong - Forbiden access, parameter is missing.", status: 403)
  end
end
