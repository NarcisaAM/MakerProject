class Api::V1::VenturesController < ApplicationController
  before_action :require_authorization

  def index
    @ventures = Venture.where(user_id: current_user.id)
    render json: @ventures
  end

  def show
    @venture = Venture.find(params[:id])
    render json: @venture
  end

  def create
    @venture = Venture.new(venture_params)
    @venture.user = current_user

    if @venture.save
      render json: @venture
    else
      render_errors(message: "Something went wrong - Paramater missing.")
    end
  end

  def destroy
    @venture = Venture.find(params[:id])
    @venture.destroy

    head 200
    # head :ok
  end

  private

    def venture_params
      params.require(:venture).permit(:name, :description)
    end
end
