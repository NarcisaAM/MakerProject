Rails.application.routes.draw do
  namespace :api do
    get "v1/status"

    namespace :v1 do
      post "/authentications", to: "authentications#create"
      post "/session", to: "session#create", defaults: { format: :json }

      get "/auth/twitter/callback", to: "omniauth_callbacks#twitter"
      get "/login_done", to: "omniauth_callbacks#login_done"

      resources :users, only: [:index, :show, :create, :edit, :destroy]

      resources :ventures, only: [:index, :show, :create, :edit, :destroy], shallow: true do
        resources :userstories
      end

    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
